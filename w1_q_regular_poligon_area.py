"""
There are several ways to calculate the area of a regular polygon.
Given the number of sides, n, and the length of each side, s, the polygon's area is

For example, a regular polygon with 5 sides, each of length 7 inches, has area 84.3033926289 square inches.
Write a function that calculates the area of a regular polygon, given the number of sides and length of each side.
Submit the area of a regular polygon with 7 sides each of length 3 inches. Enter a number (and not the units)
with at least four digits of precision after the decimal point.
"""
import math


def area_poligono(num_lados, long_lado):
    area_calc = ((1 / 4.0) * num_lados * long_lado ** 2) / (math.tan(math.pi / num_lados))
    return area_calc

print area_poligono(5, 7)
print area_poligono(7, 3)